# p6 : fichiers
import datetime
import os

nom = input("Nom ?")
nombre = int(input("Nombre de voyageurs ?"))
email = input("Email ?")

if not "@" in email:
    raise ValueError("Email faux")

try:
    fecriture = None
    # Date,Nom,Nombre,Email
    # 2024-06-12 10:19:25,Ann,4,ann@example.org
    # 2024-06-12 10:19:25,Bob,3,ann@example.org
    ligne = ( "{0:%Y}-{0:%m}-{0:%d} {0:%H}:{0:%M}:{0:%S},{1},{2},{3}\n".
        format(datetime.datetime.now(), nom, nombre, email) )
    print("Chemin : "+os.getcwd())
    existe = os.path.exists("p6fichiers.csv")        
    fecriture = open("p6fichiers.csv", "a")
    if not existe:
        fecriture.write("Date,Nom,Nombre,Email\n")
    fecriture.write(ligne)    
    print("Demande enregistrée")
except PermissionError as ex:
    print("Problème - permissions : "+str(ex))    
except OSError as ex:
    print("Problème - fichiers : "+str(ex))    
except Exception as ex:
    print("Problème inattendue : "+str(ex))
finally:
    if fecriture != None:
        fecriture.close()

print("Toutes les inscriptions :")
with open("p6fichiers.csv", "r") as flecture:
    for ligne in flecture:
        print("* "+ligne)
    