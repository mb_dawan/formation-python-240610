# p11 : Streamlit
# python -m streamlit run p11streamlit.py
import pandas
import streamlit

monuments = pandas.DataFrame( {
    "nom": ("Grande Muraille", "Alhambra", "Parthenon"),
    "pays": ("Chine", "Espagne", "Grèce"),
    "cout": (120, 34, 10)
} )

streamlit.write(monuments)
streamlit.bar_chart(data=monuments, x="nom", y="cout")