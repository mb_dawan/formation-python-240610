# p7 : fichiers XML et JSON
from xml.dom import minidom
import json

document = minidom.parse("p7voyages.xml")
print(len(document.childNodes[0].childNodes)) # 7 ?!
voyages = document.getElementsByTagName("voyage")
print("Nom du 2eme voyage : "+voyages[1].attributes["nom"].value)
liste_voyages = []
for v in voyages:
    liste_voyages.append({
        "nom": v.attributes["nom"].value, 
        "prix": v.attributes["prix"].value
    })
print(liste_voyages)

chaine_json = json.dumps(liste_voyages)
print(chaine_json)

with open("p7voyages.json", "w") as fichier_json:
     json.dump(liste_voyages, fichier_json)
