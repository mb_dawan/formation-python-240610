# P5 : bibliothèques
# 1 - mots clés (if, for, class...) : /
# 2 - fonctions, classes natives (print, str...) : /
# 3 - fonctions, classes perso : import
# 4 - fonctions, classes standard : import
# 5 - fonctions, classes tierces : pip install/conda install, import
import sys
import re
import datetime

# python p5bibliotheques.py
# python p5bibliotheques.py Martin
# python p5bibliotheques.py Martin 02389238989
#               0              1      2
if len(sys.argv)<2:
    nom = input("Nom ?")
else:
    nom = sys.argv[1]
if len(sys.argv)<3:
    tel = input("Tel ?")
else:
    tel = sys.argv[2]

# "  bob   " => "Bob"
nom = nom.strip()
nom = nom.title()
# if re.fullmatch("[0-9]{10,14}", tel)==None:
if re.fullmatch("\\+?[0-9\\.\\s]{10,14}", tel)==None:
    print("Tél erroné")

print("Demande pour "+nom+" ("+tel+") enregistrée")

maintenant = datetime.datetime.now()
depart = maintenant + datetime.timedelta(weeks=4, days=2)
print("Départ au mieux le {0:%d}/{0:%m}/{0:%Y}".format(depart))
