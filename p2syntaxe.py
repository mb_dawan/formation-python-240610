# Programme 2 : syntaxe

print("Bienvenue à l'agence")
# Canada : -20% sur 860€ d'avion et 7j à 48€
promo = 20
avion = 860
duree = int( input("Combien de jours ? ") )
hotel = 48
print("Canada :", promo, "% sur", avion, "euros d'avion et", duree, "jours à", hotel, "euros")

prix_sans_promo = avion + hotel * duree
prix = prix_sans_promo * (100 - promo) / 100
# print("Prix total : " + str(prix) + " euros") # (7j) 956.8 euros
print("Prix total : {0:.2f} euros".format(prix)) # 956.80 euros

# En général, on pars avec Air Canada, pour les voyages courts (moins de 7 jours),
# Québec'Air, et pour les voyages "réguliers" (7j, 14j, 21j, 28j), Air Transat
if duree < 7:
    companie = "Québec'Air"
elif duree % 7 == 0:
    companie = "Air Transat"
else :
    companie = "Air Canada"
print("Départ avec "+companie)    

etapes = []
for j in range(1, duree+1):
    if j == 1 or j == duree:
        occupation = "avion"
    elif j % 4 == 1:
        occupation = "poutine"
    else:
        occupation = "rando"
    etapes.append(occupation)
    print("Jour "+str(j)+" :"+occupation)

# print(etapes)
nb_randos = 0
for e in etapes:
    if e=="rando":
        nb_randos += 1
print("Nombre de randos : "+str(nb_randos))

repas = [ 
    {"prix":7.90, "contenu": 
        {"entree":"salade", "plat":"poutine", "boisson":"soda"} },     
    {"prix":19.90, "contenu": 
        {"entree":None, "plat":"homard à l'américaine", "boisson":"vin blanc"} }
]

# Afficher le plat principal du second repas
print( repas[1]["contenu"]["plat"] )
# Afficher son prix
print( repas[1]["prix"] )
# Afficher le prix total des repas avec une entrée
total_repas = 0
for r in repas:
    if r["contenu"]["entree"] != None:
        total_repas += r["prix"]
print(total_repas)