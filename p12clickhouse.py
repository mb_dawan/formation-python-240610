# p12 clickhouse
import clickhouse_connect
import logging

logging.basicConfig(filename="p12clickhouse.log.txt", 
#                    filemode="w",
                    format="%(asctime)s %(levelname)s %(message)s",
                    level=logging.INFO)

logging.info("Debut de l'application")

# port : 8123 ou 8443
conn = clickhouse_connect.get_client(host="???", port=8123, username="???", password="???")
# conn.command("DROP TABLE visites")
conn.command("CREATE TABLE IF NOT EXISTS visites(nom String, pays String, temps UInt16) "+
            "engine=MergeTree ORDER BY nom")
#conn.insert(table="visites", 
#            data=[["Arc de Triomphe", "France", 45],  ["Camp Nou", "Espagne", 120]],
#            column_names=["nom", "pays", "temps"])

temps_max = 100
curseur = conn.query("SELECT * FROM visites WHERE temps<%d", (temps_max,))
for visite in curseur.result_rows:
    print(visite[0])
conn.close()

logging.info("Fin de l'écriture et lecture")