# p8 : numpy
import numpy as np

clients = np.array(
    [ [12,5,7], # Janvier
     [35,2,8], # Février
     [2,1,5],
     [80,72,3],
     [23,24,3],
     [7,9,5],
     [81,8,15],
     [23,14,9],
     [32,7,62],
     [31,8,21],
     [9,78,6],
     [8,10,12] ], dtype=np.int16
)
print("Tout : "+str(clients))
print("Printemps : "+str(clients[3:6]))
print("1er voyage : "+str(clients[:,0]))
print("2eme et 3eme voyage : "+str(clients[:,1:3]))
print("3eme voyage de décembre : "+str(clients[11,2]))
print("Peu de clients : "+str(clients[clients<5]))
print("Forme : "+str(clients.shape)) #12, 3
clients = np.vstack( (clients, np.array([[78,8,12], [14,25,17]])) )
print("Tout (14 mois) : "+str(clients))

voyageurs = clients + 2
print("Tous : "+str(voyageurs))
print("En moyenne : "+str(np.mean(voyageurs)))