# Programme 3 : les procédures

PONCTUATION = "!"

def afficher_presentation():
    """Affiche le début des information, en particulier le pays
    """
    global PONCTUATION    
    pays = "Mexique"    
    PONCTUATION = "."
    print("Voyages au "+pays+PONCTUATION)
    
afficher_presentation()
# OK afficher_presentation()
# NON print(pays)
# OK print(PONCTUATION) # .

def afficher_danger(region:str, point:str="."):
    print("Région dangeureuse : " + region + point)

afficher_danger("Sonora", "!")
afficher_danger("Chihuahua")
afficher_danger(point=" !!", region="Chiapas")

def calculer_prix(jours, hotel, avion)->int:
    prix = jours * hotel + avion
    return prix

tarif = calculer_prix(10, 43, 980)
print("Tarif : "+str(tarif)+" €")

afficher_ville = lambda v: print("Ville "+v)
afficher_ville("Mexico")

villes = ["Mexico", "Guadalarara", "Tijuana"]
longueurs = map(len, villes)
print( list(longueurs) ) # 6, 11, 7
initiales = map(lambda v:v[0], villes)
print( list(initiales) ) # M G T