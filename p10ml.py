# p10 : ml et graphes avec scikit-learn et matplotlib
# pip install scikit-learn
from sklearn import datasets, linear_model
import matplotlib.pyplot
import numpy as np

iris = datasets.load_iris()
# print(iris)
# print(iris.data) # tableau Numpy
X = iris.data[:, :2]
Y = iris.target
matplotlib.pyplot.scatter(X[:, 0], X[:, 1], c=Y)
matplotlib.pyplot.xlabel("Longueur")
matplotlib.pyplot.ylabel("Largeur")

Xp = np.array( [ [3.2, 2.2], [7.5, 2.3], [5.2, 4.6] ]  )
logreg = linear_model.LogisticRegression()
logreg.fit(X, Y)
Yp = logreg.predict(Xp)
matplotlib.pyplot.scatter(Xp[:, 0], Xp[:, 1], c=Yp, marker="*")

matplotlib.pyplot.show()
# matplotlib.pyplot.savefig("iris.png")