# p9 : pandas
import pandas

monuments = pandas.DataFrame( {
    "nom": ("Grande Muraille", "Alhambra", "Parthenon"),
    "pays": ("Chine", "Espagne", "Grèce"),
    "cout": (120, 34, 0)
} )
print(monuments)
print("Tous les pays : "+str(monuments["pays"]))
print("Prix du dernier monument : "+str(monuments["cout"][2]))
print("Prix total : "+str(monuments["cout"].sum())) # 154
print(monuments["cout"].describe())
monuments.to_csv("p9pandas.csv")